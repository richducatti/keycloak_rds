# configured aws provider with proper credentials

data "vault_generic_secret" "aws_credentials" {
  path = "secret/aws_credentials"
}

provider "aws" {
  access_key = data.vault_generic_secret.aws_credentials.data["access_key"]
  secret_key = data.vault_generic_secret.aws_credentials.data["secret_key"]
  region     = "us-east-1"
}


# create default vpc if one does not exit
resource "aws_default_vpc" "default_vpc" {

  tags = {
    Name = "default vpc"
  }
}


# use data source to get all avalablility zones in region
data "aws_availability_zones" "available_zones" {}


# create a default subnet in the first az if one does not exit
resource "aws_default_subnet" "subnet_az1" {
  availability_zone = data.aws_availability_zones.available_zones.names[0]
}

# create a default subnet in the second az if one does not exit
resource "aws_default_subnet" "subnet_az2" {
	  availability_zone = data.aws_availability_zones.available_zones.names[1]
}

# create security group for the database
resource "aws_security_group" "database_bitnami_security_group" {
  name        = "database bitnami security group"
  description = "enable postgresql/aurora access on port 5432"
  vpc_id      = aws_default_vpc.default_vpc.id

  ingress {
    description      = "postgresql/aurora access"
    from_port        = 5432
    to_port          = 5432
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
    
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = -1
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags   = {
    Name = "database bitnami security group"
  }
}


# create the subnet group for the rds instance
resource "aws_db_subnet_group" "database_bitnami_subnet_group" {
  name         = "database-bitnami-subnets"
  subnet_ids   = [aws_default_subnet.subnet_az1.id, aws_default_subnet.subnet_az2.id]
  description  = "subnets for database instance"

  tags   = {
    Name = "database-bitnami-subnets"
  }
}


# create the rds instance
resource "aws_db_instance" "db_instance" {
  engine                  = "postgres"
  engine_version          = "14"
  multi_az                = false
  identifier              = "keycloak-bitnami"
  username                = "keycloak"
  password                = "qpalzm2024"
  instance_class          = "db.t3.micro"
  allocated_storage       = 200
  db_subnet_group_name    = aws_db_subnet_group.database_bitnami_subnet_group.name
  vpc_security_group_ids  = [aws_security_group.database_bitnami_security_group.id]
  availability_zone       = data.aws_availability_zones.available_zones.names[0]
  db_name                 = "keycloak_db"
  skip_final_snapshot     = true
  publicly_accessible     = true
}
