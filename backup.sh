#!/bin/bash

# Check if psql is installed
if ! command -v psql &> /dev/null; then
    echo "psql is not installed. Please install it before proceeding."
    exit 1
fi

# Check if .env file exists
if [ ! -f .env ]; then
    echo ".env file not found."
    exit 1
fi

# Source .env file to get database credentials
source .env

# Backup PostgreSQL database
backup_file="backup_keycloack.sql"

export PGPASSWORD="$POSTGRES_PASSWORD"
pg_dump -U "$POSTGRES_USER" -h "$DB_URL" -d "$POSTGRES_DB" > "$backup_file"

if [ $? -eq 0 ]; then
    echo "Backup completed successfully: $backup_file"
else
    echo "Backup failed."
    exit 1
fi

unset PGPASSWORD

echo "Backup down-v completed."
